#include "table.h"

#include <stdint.h>
#include <tablegen.out.h>

#include <stdio.h>
#include <limits.h>
#include <assert.h>
#include <string.h>

int
table_lookup (wchar_t src, unsigned num,
	      const wchar_t *restrict dst,
	      int *restrict out)
{
    struct redirect red;
    unsigned i, x;

    if (!src) {
	for (i = 0; i < num; i++) {
	    out[i] = table_lookup (dst[i], 0, NULL, NULL);
	}

	return 0;
    }

    x = src * REDIRECT_MUL;
    x >>= REDIRECT_SHI;
    x &= (1u << REDIRECT_MOD) - 1;

    red = redirectv[x];

    if (src == red.code) {
	for (i = 0; i < num; i++) {
	    x = dst[i];
	    x *= red.mul;
	    x >>= red.shi;
	    x &= (1u << red.mod) - 1;
	    x += red.off;

	    out[i] = dst[i] == codev[x] ?
		0 + costv[x] : dst[i] == src ?
		0 : DEFAULT_COST;
	}

	return red.ins;
    } else {
	for (i = 0; i < num; i++) {
	    out[i] = dst[i] == src ? 0 : DEFAULT_COST;
	}

	return DEFAULT_COST;
    }
}
