#include "cli.h"
#include "finder.h"
#include "input.h"

#include <signal.h>
#include <locale.h>

static void
sighandler (int signum)
{
    switch (signum) {
	case SIGINT:
	    cli_stop ();
	    break;

	case SIGWINCH:
	    cli_resize ();
	    break;

	default:
	    break;
    }
}

static int
add_entry (const char *entry, void *data)
{
    finder_add (data, entry);

    return 0;
}

int
main ()
{
    struct finder *f;
    struct input *in;

    setlocale (LC_ALL, "C.UTF-8");

    signal (SIGINT, sighandler);
    signal (SIGWINCH, sighandler);

    f = finder_create ();
    // in = input_browse (".", add_entry, f);
    in = input_read (stdin, add_entry, f);

    cli_run (f, "/dev/tty");

    finder_unref (f);
    input_wait (in);

    return 0;
}
