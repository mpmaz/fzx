#include "tree.h"

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <limits.h>
#include <assert.h>

struct treenode
{
    struct treenode *lo, *hi;
    size_t de:8, si:sizeof (size_t) * 8 - 8;
    void *data[];
};

struct tree
{
    int (*compare) (void *, void *);
    void (*destroy) (void *);
    void (*print) (void *);
    struct treenode *root;
};

[[gnu::pure]]
static unsigned
lode (struct treenode *n)
{
    return n->lo ? n->lo->de : 0;
}

[[gnu::pure]]
static unsigned
hide (struct treenode *n)
{
    return n->hi ? n->hi->de : 0;
}

[[gnu::pure]]
static size_t
losi (struct treenode *n)
{
    return n->lo ? n->lo->si : 0;
}

[[gnu::pure]]
static size_t
hisi (struct treenode *n)
{
    return n->hi ? n->hi->si : 0;
}

[[gnu::const]]
static struct treenode **
ngetl (struct treenode *t)
{
    return &t->lo;
}

[[gnu::const]]
static struct treenode **
ngeth (struct treenode *t)
{
    return &t->hi;
}

static const struct subget
{
    struct treenode
	**(*getl [[gnu::const]]) (struct treenode *),
	**(*geth [[gnu::const]]) (struct treenode *);
} sgdir = {
    .getl = ngetl,
    .geth = ngeth,
}, sgrev = {
    .getl = ngeth,
    .geth = ngetl,
};

static void
balance (struct treenode **tp)
{
    struct treenode *t, *tl, *tlh;
    struct subget sg;

    t = *tp;

    if (!t) {
	return;
    }

    t->de = (lode (t) > hide (t) ? lode (t) : hide (t)) + 1;
    t->si = losi (t) + hisi (t) + 1;

    if (lode (t) > hide (t) + 1) {
	sg = sgdir;
    } else if (hide (t) > lode (t) + 1) {
	sg = sgrev;
    } else {
	return;
    }

    tl = *sg.getl (t);
    tlh = *sg.geth (tl);

    if (tlh) {
	*tp = tlh;
	*sg.getl (t) = *sg.geth (tlh);
	*sg.geth (tl) = *sg.getl (tlh);
	*sg.geth (tlh) = t;
	*sg.getl (tlh) = tl;
    } else {
	*tp = tl;
	*sg.geth (tl) = t;
	*sg.getl (t) = NULL;
    }

    balance (sg.geth (*tp));
    balance (sg.getl (*tp));
    balance (tp);
}

static int
compare_append (void *e0, void *e1)
{
    return 1;
}

struct tree *
tree_create (int (*compare) (void *, void *),
	     void (*destroy) (void *),
	     void (*print) (void *))
{
    struct tree *t;

    if (!compare) {
	compare = compare_append;
    }

    t = malloc (sizeof (struct tree));
    t->compare = compare;
    t->destroy = destroy;
    t->print = print;
    t->root = NULL;

    return t;
}

void
tree_destroy (struct tree *t)
{
    treenode_destroy (t->root, t);

    free (t);
}

static bool
node_add (struct tree *t, struct treenode **np,
	  struct treenode **nn,
	  void *nndata,
	  struct treenode *(*new) (void *), void *newdata,
	  size_t *pos)
{
    struct treenode *n, **(*next [[gnu::const]]) (struct treenode *);
    int cmp;

    if (!(n = *np)) {
	if (*nn) {
	    *np = *nn;
	} else if (new) {
	    *np = *nn = new (newdata);
	}

	return true;
    }

    cmp = t->compare (nndata, treenode_data (n));

    if (cmp < 0) {
	next = ngetl;
    } else if (cmp > 0) {
	(*pos) += losi (n) + 1;

	next = ngeth;
    } else {
	(*pos) += losi (n);

	*nn = n;

	return false;
    }

    if (node_add (t, next (n), nn, nndata, new, newdata, pos)) {
	balance (np);

	return true;
    }

    return false;
}

size_t
tree_addnode (struct tree *t, struct treenode *n)
{
    size_t pos = 0;

    if (!node_add (t, &t->root, &n, treenode_data (n), NULL, NULL, &pos)) {
	treenode_destroy (n, t);
    }

    return pos;
}

static struct treenode *
addnew (void *dp)
{
    void **d = dp;

    return treenode_create (d[0], *(size_t *) d[1]);;
}

size_t
tree_add (struct tree *t, void *data, size_t datasize)
{
    struct treenode *n = NULL;
    size_t pos = 0;
    void *newdata[] = { data, &datasize };

    if (node_add (t, &t->root, &n, data, addnew, newdata, &pos)) {
	assert (n);
    } else if (t->destroy) {
	t->destroy (data);
    }

    return pos;
}

void *
tree_lazy (struct tree *t,
	   void *data,
	   struct treenode *(*new) (void *),
	   void *newdata)
{
    struct treenode *n = NULL;
    size_t pos = 0;

    node_add (t, &t->root, &n, data, new, newdata, &pos);
    assert (n);

    return treenode_data (n);
}

struct treenode *
tree_findnode (struct tree *t, void *data, size_t *pos)
{
    struct treenode *n = NULL;
    size_t p = 0;

    node_add (t, &t->root, &n, data, NULL, NULL, &p);

    if (pos) {
	*pos = p;
    }

    return n;
}

void *
tree_find (struct tree *t, void *data, size_t *pos)
{
    return treenode_data (tree_findnode (t, data, pos));
}

static void *
node_data (struct treenode *n, size_t id)
{
    if (!n) {
	return NULL;
    }

    if (id < n->si - hisi (n) - 1) {
	return node_data (n->lo, id);
    } else if (id > n->si - hisi (n) - 1) {
	return node_data (n->hi, id - losi (n) - 1);
    } else {
	return treenode_data (n);
    }
}

void *
tree_data (struct tree *t, size_t id)
{
    return node_data (t->root, id);
}

size_t
tree_size (struct tree *t)
{
    return t->root ? t->root->si : 0;
}

static struct treenode *
stealut (struct treenode **np, struct subget sg)
{
    struct treenode *n;

    if (!(n = *np) || (n = stealut (sg.getl (n), sg))) {
	balance (np);

	return n;
    }

    *np = *sg.geth (n = *np);

    return n;
}

static void
node_init (struct treenode *n)
{
    n->lo = n->hi = NULL;
    n->si = n->de = 1;
}

static size_t
node_cut (struct treenode **np, struct treenode **nv,
	  size_t index, size_t count)
{
    struct treenode *n;
    struct subget sg;
    size_t num;
    bool detach;

    if (!(n = *np)) {
	return 0;
    }

    if (index < losi (n)) {
	num = node_cut (&n->lo, nv, index, count);
	index = 0;
    } else {
	num = 0;
	index -= losi (n);
    }

    if (num < count) {
	if ((detach =! index)) {
	    nv[num++] = n;
	} else {
	    index--;
	}

	if (num < count) {
	    num += node_cut (&n->hi, nv + num, index, count - num);
	}

	if (detach) {
	    if (!n->lo) {
		//printf ("collapse to hi\n");
		*np = n->hi;
	    } else if (!n->hi) {
		//printf ("collapse to lo\n");
		*np = n->lo;
	    } else {
		//printf ("hi and lo nonnull\n");

		assert (n->lo);
		assert (n->hi);

		sg = losi (n) < hisi (n) ? sgdir : sgrev;

		assert (*sg.getl (n));
		assert (*sg.geth (n));

		*np = stealut (sg.geth (n), sg);

		(*np)->lo = n->lo;
		(*np)->hi = n->hi;
	    }

	    node_init (n);
	}
    }

    balance (np);

    return num;
}

static void
foreach (struct treenode *n, void (*iter) (void *, void *), void *user)
{
    if (n) {
	foreach (n->lo, iter, user);
	iter (treenode_data (n), user);
	foreach (n->hi, iter, user);
    }
}

void
tree_foreach (struct tree *t, void (*iter) (void *, void *), void *user)
{
    foreach (t->root, iter, user);
}

size_t
tree_cutnode (struct tree *t, struct treenode **nv, size_t index, size_t count)
{
    return node_cut (&t->root, nv, index, count);
}

void
tree_delete (struct tree *t, size_t index)
{
    struct treenode *n;

    if (tree_cutnode (t, &n, index, 1)) {
	treenode_destroy (n, t);
    }
}

static struct treenode *
node_assert (struct treenode *n, size_t pos)
{
    if (!n) {
	return n;
    }

    node_assert (n->lo, pos);
    node_assert (n->hi, pos + losi (n) + 1);

    pos = pos + losi (n);

    assert (n->de == (lode (n) > hide (n) ? lode (n) : hide (n)) + 1);
    assert (n->si == losi (n) + hisi (n) + 1);

    if (lode (n) + 1 < hide (n)) {
	fprintf (stderr, "too small ld %lu\n", pos);
	exit (EXIT_FAILURE);
    }

    if (hide (n) + 1 < lode (n)) {
	fprintf (stderr, "too small hd %lu\n", pos);
	exit (EXIT_FAILURE);
    }

    return n;
}

void
tree_assert (struct tree *t)
{
    node_assert (t->root, 0);
}

static void
node_print (struct tree *t, struct treenode *n, int indent, unsigned d)
{
    unsigned i;

    if (!n) {
	return;
    }

    node_print (t, n->lo, indent, d + 1);

    for (i = 0; i < d * indent; i++) {
	printf ("  ");
    }

    if (t->print) {
	t->print (treenode_data (n));
    } else {
	printf ("%p", treenode_data (n));
    }

    node_print (t, n->hi, indent, d + 1);
}

void
tree_print (struct tree *t, int indent)
{
    node_print (t, t->root, indent, 0);
}

struct treenode *
treenode_create (void *data, size_t datasize)
{
    struct treenode *n;

    n = malloc (sizeof (struct treenode) + datasize);
    node_init (n);

    if (data) {
	memcpy (treenode_data (n), data, datasize);
    } else {
	memset (treenode_data (n), 0, datasize);
    }

    return n;
}

void
treenode_destroy (struct treenode *n, struct tree *t)
{
    if (!n) {
	return;
    }

    treenode_destroy (n->lo, t);
    treenode_destroy (n->hi, t);

    if (t->destroy) {
	t->destroy (treenode_data (n));
    }

    free (n);
}

void *
treenode_data (struct treenode *n)
{
    return n ? n->data : NULL;
}
