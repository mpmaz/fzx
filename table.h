#pragma once

#include <stddef.h>

int table_lookup (wchar_t src, unsigned num,
		  const wchar_t *restrict dst,
		  int *restrict out);
