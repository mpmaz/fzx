#pragma once

#include <stddef.h>
#include <stdbool.h>

struct finder;

struct finder *finder_create ();
struct finder *finder_ref (struct finder *f);
void finder_unref (struct finder *f);
void finder_listen (struct finder *f,
		    void (*insert) (size_t, void *),
		    void (*clear) (void *),
		    void *data);
void finder_unlisten (struct finder *f,
		      void (*insert) (size_t, void *),
		      void (*clear) (void *),
		      void *data);
void finder_add (struct finder *f, const char *entry);
void finder_search (struct finder *f, const char *search);
size_t finder_load (struct finder *f, size_t count,
		    const char **entryv, int *scorev);
size_t finder_read (struct finder *f, size_t index, size_t count,
		    bool (*func) (const char *, void *), void *data);
