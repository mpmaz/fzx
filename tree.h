#pragma once

#include <stddef.h>

struct tree;
struct treenode;

struct tree *
tree_create (int (*compare) (void *, void *),
	     void (*destroy) (void *),
	     void (*print) (void *));

void
tree_destroy (struct tree *t);

size_t
tree_addnode (struct tree *t,
	      struct treenode *n);

size_t
tree_add (struct tree *t,
	  void *data,
	  size_t datasize);

void *
tree_lazy (struct tree *t,
	   void *data,
	   struct treenode *(*new) (void *),
	   void *newdata);

struct treenode *
tree_findnode (struct tree *t,
	       void *data,
	       size_t *pos);

void *
tree_find (struct tree *t,
	   void *data,
	   size_t *pos);

[[gnu::pure]] void *
tree_data (struct tree *t, size_t id);

[[gnu::pure]] size_t
tree_size (struct tree *t);

void
tree_foreach (struct tree *t,
	      void (*iter) (void *, void *),
	      void *user);

size_t
tree_cutnode (struct tree *t,
	      struct treenode **nv,
	      size_t index, size_t count);

void
tree_delete (struct tree *t,
	     size_t index);

void
tree_assert (struct tree *t);

void
tree_print (struct tree *t, int indent);

struct treenode *
treenode_create (void *data,
		 size_t datasize);

void
treenode_destroy (struct treenode *n,
		  struct tree *t);

[[gnu::pure]] void *
treenode_data (struct treenode *n);
