#include "cli.h"
#include "tty.h"
#include "finder.h"

#include <string.h>
#include <assert.h>
#include <pthread.h>
#include <signal.h>
#include <ctype.h>

static volatile int run;
static pthread_mutex_t mtx;
static pthread_cond_t cnd;

enum
{
    RESIZE = 1,
    REDRAW = 4,

    UISIZE = 21,
    LINESIZE = 256,
};

static unsigned flags;
static int width, height, column, row;

static struct finder *f;
static struct tty *t;
static char line[LINESIZE];
static int linelen;

static bool
readraw_iter (const char *s, void *ud)
{
    tty_puts (t, "\n");
    tty_clearline (t);
    tty_puts (t, s);

    return true;
}

static void
redraw ()
{
    size_t num;

    num = finder_read (f, 0, UISIZE - 1,
		       readraw_iter, NULL);

    while (num++ < UISIZE - 1) {
	tty_puts (t, "\n");
	tty_clearline (t);
    }

    tty_move (t, UISIZE);
    tty_puts (t, "\r");
    tty_clearline (t);
    tty_puts (t, line);
}

static void
on_insert (size_t index, void *p)
{
    cli_update (index);
}

static void
feed (int c, void *unused)
{
    pthread_mutex_lock (&mtx);

    switch (c) {
    case 3:
	cli_stop ();
	break;

    case '\b':
    case 127:
	if (linelen) {
	    line[--linelen] = 0;
	    flags |= REDRAW;
	    finder_search (f, line);
	}
	break;

    default:
	if ((isdigit (c) || isalpha (c)) && linelen < LINESIZE) {
	    line[linelen++] = c;
	    flags |= REDRAW;
	    finder_search (f, line);
	}
    }

    pthread_cond_signal (&cnd);
    pthread_mutex_unlock (&mtx);
}

void
cli_run (struct finder *fn, const char *path)
{
    int i;

    f = fn;

    finder_listen (f, on_insert, NULL, NULL);

    pthread_mutex_init (&mtx, NULL);
    pthread_cond_init (&cnd, NULL);

    t = tty_open (path, feed, NULL);

    run = 1;
    flags = 0;

    tty_getsize (t, &width, &height);
    tty_getpos (t, &column, &row);

    assert (column == 0);

    if (height - row < UISIZE) {
	tty_scroll (t, UISIZE - (height - row));
	tty_move (t, UISIZE - (height - row));
    }

    pthread_mutex_lock (&mtx);

    while (run) {
	if (flags & REDRAW) {
	    flags &= ~REDRAW;
	    redraw ();
	} else {
	    pthread_cond_wait (&cnd, &mtx);
	}
    }

    tty_puts (t, "\r");
    tty_clearline (t);

    for (i = 0; i < UISIZE - 1; i++) {
	tty_puts (t, "\n");
	tty_clearline (t);
    }

    tty_move (t, UISIZE - 1);
    tty_close (t);

    pthread_mutex_unlock (&mtx);

    pthread_mutex_destroy (&mtx);
    pthread_cond_destroy (&cnd);
}

void
cli_stop ()
{
    run = 0;

    pthread_cond_signal (&cnd);
}

void
cli_resize ()
{
    return;
    pthread_mutex_lock (&mtx);

    tty_getsize (t, &width, &height);

    flags |= REDRAW;

    pthread_cond_signal (&cnd);
    pthread_mutex_unlock (&mtx);
}

void
cli_update (size_t index)
{
    pthread_mutex_lock (&mtx);

    if (index < 10) {
	flags |= REDRAW;

	pthread_cond_signal (&cnd);
    }

    pthread_mutex_unlock (&mtx);
}
