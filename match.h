#pragma once

#include <stddef.h>

int match (int textlen, const wchar_t *text,
	   int searchlen, const wchar_t *search);
