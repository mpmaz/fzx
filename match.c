#include "match.h"
#include "table.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <wchar.h>
#include <assert.h>

#define INTROSPECT 0

int
match (int textlen, const wchar_t *text,
       int searchlen, const wchar_t *search)
{
    int **table, *textcost, *searchcost, x, y, ci, cd, cs;

    table = alloca (sizeof (int *) * (searchlen + 1));

    for (y = 0; y < searchlen + 1; y++) {
	table[y] = alloca (sizeof (int) * (textlen + 1));
    }

    textcost = alloca (sizeof (int) * textlen);
    searchcost = alloca (sizeof (int) * searchlen);

    table_lookup (0, textlen, text, textcost);

    table[0][0] = 0;

    for (x = 0; x < textlen; x++) {
	table[0][x + 1] = table[0][x] + textcost[x];
	// table[0][x + 1] = x * 10;
#if INTROSPECT > 1
	printf ("delete %lc %d\n", text[x], table[0][x + 1]);
#endif
    }

#if INTROSPECT > 1
    printf ("--\n");
#endif

    for (y = 0; y < searchlen; y++) {
	searchcost[y] = table_lookup (search[y], textlen,
				      text, &table[y + 1][1]);
	table[y + 1][0] = table[y][0] + searchcost[y] * 10;
#if INTROSPECT > 1
	printf ("insert %lc %d\n", search[y], table[y + 1][0]);
#endif

	for (x = 0; x < textlen; x++) {
	    cd = table[y + 1][x] + textcost[x];
	    ci = table[y][x + 1] + searchcost[y] * 16;
	    cs = table[y][x] + table[y + 1][x + 1] * 8;

	    table[y + 1][x + 1] =
		cd < ci ? cd < cs ? cd : cs : ci < cs ? ci : cs;

#if INTROSPECT > 1
	    printf ("replace %lc %lc %d | %d %d %d\n",
		    search[y], text[x], table[y + 1][x + 1],
		    cd, ci, cs);
#endif
	}
    }

#if INTROSPECT
    printf ("      ");

    for (x = 0; x < textlen; x++) {
	printf ("    %lc", text[x]);
    }

    printf ("\n");

    for (y = 0; y <= searchlen; y++) {
	printf ("%lc ", y ? search[y - 1] : L' ');

	for (x = 0; x <= textlen; x++) {
	    printf ("%4d ", table[y][x]);
	}

	printf ("\n");
    }

    printf ("\n---------------------------------\n");
#endif

    return table[searchlen][textlen];
}
