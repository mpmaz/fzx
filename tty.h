#pragma once

enum color
{
    COLOR_BLACK = 0,
    COLOR_RED = 1,
    COLOR_GREEN = 2,
    COLOR_YELLOW = 3,
    COLOR_BLUE = 4,
    COLOR_MAGENTA = 5,
    COLOR_CYAN = 6,
    COLOR_WHITE = 7,
    COLOR_NORMAL = 9,
};

struct tty;

struct tty *tty_open (const char *dev,
		      void (*feed) (int, void *),
		      void *data);
void tty_close (struct tty *t);
void tty_getsize (struct tty *t, int *w, int *h);
void tty_getpos (struct tty *t, int *x, int *y);
void tty_printf (struct tty *t, const char *fmt, ...);
void tty_puts (struct tty *t, const char *str);
void tty_scroll (struct tty *t, int scroll);
void tty_move (struct tty *t, int move);
void tty_clearline (struct tty *t);
void tty_column (struct tty *t, int col);
