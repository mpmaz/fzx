#pragma once

#include <stddef.h>

struct finder;

void cli_run (struct finder *f, const char *path);
void cli_stop ();
void cli_resize ();
void cli_update (size_t index);
