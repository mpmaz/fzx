#pragma once

#include <stdio.h>

struct input;

struct input *input_browse (const char *root,
			    int (*func) (const char *, void *),
			    void *usrdata);
struct input *input_read (FILE *file,
			  int (*func) (const char *, void *),
			  void *usrdata);
void input_wait (struct input *in);
void input_close (struct input *in);
