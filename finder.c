#include "finder.h"
#include "match.h"
#include "cli.h"
#include "tree.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <wchar.h>
#include <assert.h>
#include <pthread.h>

enum
{
    BLOCK_SIZE = 16,
    NUM_THREADS = 1,
};

struct entry
{
    int score;
    char str[];
};

struct listen
{
    struct listen *next;
    void (*insert) (size_t, void *);
    void (*clear) (void *);
    void *data;
};

struct search
{
    unsigned ref;
    bool valid;
    char *str;
    wchar_t wcs[];
};

struct newblock
{
    struct newblock *next;
    unsigned newc;
    struct treenode *newv[BLOCK_SIZE];
};

struct treequeue
{
    struct treequeue *next;
    struct tree *tree;
};

static struct search *
search_alloc (const char *str)
{
    struct search *r;
    const char *wsrc;
    wchar_t *wcs;
    size_t slen, wlen;

    slen = strlen (str);
    wcs = alloca (sizeof (wchar_t) * (slen + 1));

    wsrc = str;
    wlen = mbsrtowcs (wcs, &wsrc, slen + 1, NULL);

    if (wlen == (size_t) -1) {
	perror (__func__);
	exit (EXIT_FAILURE);
    }

    r = malloc (sizeof (struct search) +
		sizeof (wchar_t) * (wlen + 1) +
		slen + 1);
    r->ref = 1;
    r->valid = true;
    r->str = (char *) (r->wcs + wlen + 1);
    wcscpy (r->wcs, wcs);
    strcpy (r->str, str);

    assert (r->wcs[wlen] == 0);
    assert (r->str[slen] == 0);

    return r;
}

static struct search *
search_ref (struct search *r)
{
    if (r) {
	r->ref++;
    }

    return r;
}

static struct search *
search_unref (struct search *r)
{
    if (r && !--r->ref) {
	free (r);
	r = NULL;
    }

    return r;
}

struct finder
{
    unsigned ref;

    volatile bool run;

    struct listen *listen;

    struct tree *tree;
    struct search *search;
    struct newblock *newhead;
    struct newblock *newtail;
    struct treequeue *queuehead;
    struct treequeue **queueiter;

    pthread_mutex_t listenlock;
    pthread_mutex_t treelock;
    pthread_mutex_t searchlock;
    pthread_mutex_t newlock;
    pthread_mutex_t queuelock;
    pthread_cond_t waitcond;

    pthread_t threads[NUM_THREADS];
};

static int
compare_entry (void *e0p, void *e1p)
{
    struct entry *e0, *e1;

    e0 = e0p;
    e1 = e1p;

    if (e0->score < e1->score) {
	return -1;
    }

    if (e0->score > e1->score) {
	return 1;
    }

    return strcmp (e0->str, e1->str);
}

static void
print_entry (void *ep)
{
    struct entry *e = ep;

    printf ("%s", e->str);
}

static unsigned
dequeue (struct finder *f, unsigned ncmax, struct treenode **nv)
{
    struct newblock *head;
    struct treequeue *it;
    size_t cut;
    unsigned nc, c;

    nc = 0;

    if (f->newhead) {
	pthread_mutex_lock (&f->newlock);

	while ((head = f->newhead) && nc < ncmax) {
	    assert (head->newc);
	    assert (head->newc <= BLOCK_SIZE);

	    c = ncmax - nc;

	    if (c > head->newc) {
		c = head->newc;
	    }

	    memcpy (nv, head->newv,
		    sizeof (struct treenode *) * c);
	    memmove (head->newv, head->newv + c,
		     sizeof (struct treenode *) * (head->newc - c));
	    head->newc -= c;
	    nc += c;

	    if (!head->newc) {
		if (head == f->newtail) {
		    assert (!head->next);
		    f->newhead = f->newtail = NULL;
		} else {
		    assert (head->next);
		    f->newhead = head->next;
		}

		free (head);
	    }
	}

	pthread_mutex_unlock (&f->newlock);
    }

    if (f->queueiter) {
	pthread_mutex_lock (&f->queuelock);

	while (nc < ncmax && (it = *f->queueiter)) {
	    cut = tree_cutnode (it->tree, nv + nc, 0, ncmax - nc);

	    if (!tree_size (it->tree)) {
		*f->queueiter = it->next;

		tree_destroy (it->tree);
		free (it);
	    }

	    nc += cut;
	}

	pthread_mutex_unlock (&f->queuelock);
    }

    return nc;
}

static void
compute (struct finder *f, const wchar_t *search,
	 unsigned nc, struct treenode **nv)
{
    struct entry *e;
    size_t wlen;
    wchar_t *wcs;
    const char *wsrc;
    unsigned i;

    for (i = 0; i < nc; i++) {
	e = treenode_data (nv[i]);

	wlen = strlen (wsrc = e->str);
	wcs = malloc (sizeof (wchar_t) * (wlen + 1));
	wlen = mbsrtowcs (wcs, &wsrc, wlen + 1, NULL);
	assert (wlen != (size_t) -1);

	e->score = match (wlen, wcs, wcslen (search), search);

	free (wcs);
    }
}

static void *
thread (void *fp)
{
    pthread_mutex_t waitlock;
    struct finder *f;
    struct treenode *nv;
    struct search *search;
    struct listen *l;
    unsigned nc;
    size_t pos;

    f = fp;
    nc = 0;

    pthread_mutex_init (&waitlock, NULL);
    pthread_mutex_lock (&waitlock);

    while (f->run) {
	if (nc || (nc = dequeue (f, 1, &nv))) {
	    assert (nc == 1);

	    pthread_mutex_lock (&f->searchlock);
	    search = search_ref (f->search);
	    pthread_mutex_unlock (&f->searchlock);

	    compute (f, search->wcs, nc, &nv);

	    pthread_mutex_lock (&f->treelock);

	    if (search->valid) {
		pos = tree_addnode (f->tree, nv);
		nc = 0;
	    }

	    search_unref (search);
	    pthread_mutex_unlock (&f->treelock);

	    if (!nc) {
		l = f->listen;

		pthread_mutex_lock (&f->listenlock);

		while (l) {
		    if (l->insert) {
			l->insert (pos, l->data);
		    }

		    l = l->next;
		}

		pthread_mutex_unlock (&f->listenlock);
	    }
	} else {
	    pthread_cond_wait (&f->waitcond, &waitlock);
	}
    }

    if (nc) {
	treenode_destroy (nv, f->tree);
    }

    pthread_mutex_unlock (&waitlock);
    pthread_mutex_destroy (&waitlock);

    return NULL;
}

struct finder *
finder_create ()
{
    struct finder *f;
    unsigned i;

    f = malloc (sizeof (struct finder));
    memset (f, 0, sizeof (struct finder));

    f->run = true;
    f->tree = tree_create (compare_entry, NULL, print_entry);
    f->search = search_alloc ("");
    f->queueiter = &f->queuehead;

    pthread_mutex_init (&f->listenlock, NULL);
    pthread_mutex_init (&f->treelock, NULL);
    pthread_mutex_init (&f->searchlock, NULL);
    pthread_mutex_init (&f->newlock, NULL);
    pthread_mutex_init (&f->queuelock, NULL);
    pthread_cond_init (&f->waitcond, NULL);

    for (i = 0; i < NUM_THREADS; i++) {
	if (pthread_create (&f->threads[i], NULL, thread, f)) {
	    perror (__func__);
	    exit (EXIT_FAILURE);
	}
    }

    return f;
}

void
finder_unref (struct finder *f)
{
    struct listen *l;
    unsigned i;

    if (!f || --f->ref) {
	return;
    }

    pthread_mutex_lock (&f->queuelock);

    f->run = false;

    pthread_cond_broadcast (&f->waitcond);
    pthread_mutex_unlock (&f->queuelock);

    for (i = 0; i < NUM_THREADS; i++) {
	if (pthread_join (f->threads[i], NULL)) {
	    perror (__func__);
	    exit (EXIT_FAILURE);
	}
    }

    search_unref (f->search);

    pthread_mutex_destroy (&f->listenlock);
    pthread_mutex_destroy (&f->treelock);
    pthread_mutex_destroy (&f->searchlock);
    pthread_mutex_destroy (&f->newlock);
    pthread_mutex_destroy (&f->queuelock);
    pthread_cond_destroy (&f->waitcond);

    while ((l = f->listen)) {
	f->listen = l->next;
	free (l);
    }

    free (f);
}

void
finder_listen (struct finder *f,
	       void (*insert) (size_t, void *),
	       void (*clear) (void *),
	       void *data)
{
    struct listen **l;

    l = &f->listen;

    pthread_mutex_lock (&f->listenlock);

    while (*l) {
	l = &(*l)->next;
    }

    (*l) = malloc (sizeof (struct listen));
    (*l)->next = NULL;
    (*l)->insert = insert;
    (*l)->clear = clear;
    (*l)->data = data;

    pthread_mutex_unlock (&f->listenlock);
}

void
finder_unlisten (struct finder *f,
		 void (*insert) (size_t, void *),
		 void (*clear) (void *),
		 void *data)
{
    struct listen **l, *p;

    l = &f->listen;

    pthread_mutex_lock (&f->listenlock);

    while ((p = *l)) {
	if (p->data == data) {
	    if (p->insert == insert) {
		p->insert = NULL;
	    }
	    
	    if (p->clear == clear) {
		p->clear = NULL;
	    }

	    if (!p->insert && !p->clear) {
		*l = p->next;
		free (p);
		continue;
	    }
	}

	l = &p->next;
    }

    pthread_mutex_unlock (&f->listenlock);
}

void
finder_add (struct finder *f, const char *entry)
{
    struct treenode *n;
    struct entry *e;

    if (!entry || !strlen (entry)) {
	return;
    }

    pthread_mutex_lock (&f->newlock);

    n = treenode_create (NULL, sizeof (struct entry) + strlen (entry) + 1);
    e = treenode_data (n);
    e->score = 0;
    strcpy (e->str, entry);

    assert (!f->newtail == !f->newhead);

    if (!f->newtail) {
	f->newhead = f->newtail = malloc (sizeof (struct newblock));
	f->newhead->next = NULL;
	f->newhead->newc = 0;
    } else if (f->newtail->newc == BLOCK_SIZE) {
	f->newtail = (f->newtail->next = malloc (sizeof (struct newblock)));
	f->newtail->next = NULL;
	f->newtail->newc = 0;
    }

    f->newtail->newv[f->newtail->newc++] = n;

    pthread_cond_signal (&f->waitcond);
    pthread_mutex_unlock (&f->newlock);
}

void
finder_search (struct finder *f, const char *search)
{
    struct treequeue *q;

    if (!search) {
	search = "";
    }

    pthread_mutex_lock (&f->searchlock);

    if (strcmp (search, f->search->str)) {
	if (f->search) {
	    f->search->valid = false;
	    search_unref (f->search);
	}

	f->search = search_alloc (search);

	pthread_mutex_lock (&f->treelock);
	pthread_mutex_lock (&f->queuelock);

	q = malloc (sizeof (struct treequeue));
	q->next = f->queuehead;
	q->tree = f->tree;

	f->tree = tree_create (compare_entry, NULL, print_entry);
	f->queuehead = q;

	pthread_cond_broadcast (&f->waitcond);
	pthread_mutex_unlock (&f->queuelock);
	pthread_mutex_unlock (&f->treelock);
    }

    pthread_mutex_unlock (&f->searchlock);
}

size_t
finder_loada (struct finder *f, size_t count,
	     const char **entryv, int *scorev)
{
    struct entry *e;
    size_t i;

    pthread_mutex_lock (&f->treelock);

    if (count > tree_size (f->tree)) {
	count = tree_size (f->tree);
    }

    for (i = 0; i < count; i++) {
	e = tree_data (f->tree, i);

	assert (e);

	if (entryv) {
	    entryv[i] = e->str;
	}

	if (scorev) {
	    scorev[i] = e->score;
	}
    }

    pthread_mutex_unlock (&f->treelock);

    return count;
}

size_t
finder_read (struct finder *f, size_t index, size_t count,
	     bool (*func) (const char *, void *), void *data)
{
    struct entry *e;
    size_t i;

    pthread_mutex_lock (&f->treelock);

    if (count > tree_size (f->tree)) {
	count = tree_size (f->tree);
    }

    for (i = 0; i < count; i++) {
	e = tree_data (f->tree, i);
	assert (e);

	if (!func (e->str, data)) {
	    break;
	}
    }

    pthread_mutex_unlock (&f->treelock);

    return i;
}
