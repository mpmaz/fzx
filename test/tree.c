#include <tree.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

static void
print_string (void *s)
{
    printf ("%s\n", (char *) s);
}

static void
test_add (unsigned N)
{
    struct tree *t;
    char e[N][16], *p;
    unsigned i;
    size_t pos;

    t = tree_create ((void *) strcmp, NULL, print_string);

    for (i = 0; i < N; i++) {
	assert (tree_size (t) == i);

	snprintf (e[i], sizeof (e[i]), "test-%u", i + 1);
	pos = tree_addnode (t, treenode_create (e[i], strlen (e[i]) + 1));
	printf ("add %s %lu\n", e[i], pos);

	assert (t);
	assert (pos < tree_size (t));

	p = tree_data (t, pos);

	assert (!strcmp (p, e[i]));
	tree_print (t, 1);
	printf ("\n");
    }

    tree_print (t, 1);

    assert (tree_size (t) == N);

    qsort (e, N, sizeof (e[0]), (void *) strcmp);

    for (i = 0; i < N; i++) {
	p = tree_data (t, i);
	assert (!strcmp (p, e[i]));
    }

    tree_destroy (t);
}

static void
test_cut (unsigned N)
{
    struct tree *t;
    unsigned i;
    char e[16];
    struct treenode *c[N];
    size_t cn;

    t = tree_create ((void *) strcmp, NULL, print_string);

    for (i = 0; i < N; i++) {
	snprintf (e, sizeof (e), "test%u", i + 1);
	tree_addnode (t, treenode_create (e, strlen (e) + 1));
    }

    tree_print (t, 1);
    tree_assert (t);

    cn = tree_cutnode (t, c, 10, 30);

    assert (cn == 30);

    printf ("\ncut %lu\n", cn);

    for (i = 0; i < cn; i++) {
	printf ("cut %u: %s\n", i, (char *) treenode_data (c[i]));
	treenode_destroy (c[i], t);
    }

    printf ("\n");

    assert (tree_size (t) == N - cn);

    for (i = 1; i < tree_size (t); i++) {
	assert (strcmp (tree_data (t, i - 1), tree_data (t, i)) < 0);
    }

    tree_print (t, 1);
    tree_assert (t);

    tree_destroy (t);

    printf ("all done\n");
}

int
main ()
{
    test_add (10);
    test_add (100);
    test_cut (100);

    unsigned i;
    char e[16];
    struct tree *t;

    t = tree_create ((void *) strcmp, NULL, print_string);

    for (i = 0; i < 0; i++) {
	snprintf (e, sizeof (e), "test%u", i);
	tree_addnode (t, treenode_create (e, strlen (e) + 1));
    }

    tree_print (t, 1);
    tree_destroy (t);

    return 0;
}
