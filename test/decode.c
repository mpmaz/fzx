#include <decode.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

int
main ()
{
    const char *in;
    wchar_t *out;

    in = "Välkommen till Luleå";
    out = alloca (sizeof (wchar_t) * 64);

    decode (1, &out, &in);

    return 0;
}
