#include <match.h>
#include <wchar.h>
#include <stdio.h>
#include <locale.h>

int
main ()
{
    const wchar_t entry[] = L"hello";
    const wchar_t search[] = L"helpme";

    setlocale (LC_ALL, "C.UTF8");

    match (wcslen (entry), entry, wcslen (search), search);

    printf ("done\n");

    return 0;
}
