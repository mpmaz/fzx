#include <graph.h>

#include <stdio.h>
#include <assert.h>

int
main ()
{
    struct graph *g;
    int i;

    g = graph_create ();

    graph_set (g, 0, 1, 10);
    assert (graph_get (g, 0, 1) == 10);
    assert (graph_get (g, 1, 0) == 10);
    //assert (graph_get (g, 0, 0) == -1);
    graph_set (g, 0, 2, 20);
    graph_set (g, 1, 3, 20);
    assert (graph_size (g) == 4);

    graph_dijkstra (g, 0);

    for (i = 0; i < graph_size (g); i++) {
	printf ("dist %d: %d\n",
		graph_node (g, i),
		graph_dist (g, graph_node (g, i)));
    }

    assert (graph_dist (g, 0) == 0);
    assert (graph_dist (g, 1) == 10);
    assert (graph_dist (g, 2) == 20);
    assert (graph_dist (g, 3) == 30);

    graph_print (g);

    graph_destroy (g);

    return 0;
}
