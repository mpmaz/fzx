#include <tablegen.h>
#include <table.h>
#include <tablegen.out.h>

#include <stdlib.h>
#include <limits.h>
#include <locale.h>
#include <assert.h>

int
main ()
{
    unsigned i0, i1, expect, cost;
    int value;
    struct entry *e;
    FILE *file;

    setlocale (LC_ALL, "C.UTF8");

    file = fopen ("../tablegen.in", "r");
    assert (file);

    e = gentable (file, NULL);
    fclose (file);

    assert (e);

    for (i0 = 0; i0 < e->size; i0++) {
	for (i1 = 0; i1 < e->list[i0].size; i1++) {
	    expect = e->list[i0].list[i1].cost;

	    cost = table_lookup (e->list[i0].code, 1,
				 &e->list[i0].list[i1].code,
				 &value);

	    /*
	    printf ("check table %lc %lc %u %u\n",
		    e->list[i0].code,
		    e->list[i0].list[i1].code,
		    expect, value);
		    */
	    assert (e->list[i0].cost == cost);
	    assert (expect == value);
	}
    }

    printf ("test passed successfully\n");

    free (e);

    return 0;
}
