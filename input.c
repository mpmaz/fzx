#include "input.h"

#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <linux/limits.h>

struct input
{
    pthread_t thread;
    int (*func) (const char *, void *);
    void *usrdata;
    volatile int stop;
    [[gnu::aligned (sizeof (void *))]] char extra[];
};

struct bfs
{
    struct bfs *next;
    char path[];
};

static void *
browse_thread (void *inp)
{
    struct input *in;
    struct bfs *qhead, *qtail, *l;
    struct dirent *de;
    DIR *dir;
    char *root, fpath[PATH_MAX + 1];
    int addslash;

    in = inp;
    dir = NULL;
    root = in->extra;
    addslash = root[strlen (root) - 1] != '/';

    qhead = malloc (sizeof (struct bfs) + strlen (root) + addslash + 1);
    qhead->next = NULL;
    strcpy (qhead->path, root);

    if (addslash) {
	strcat (qhead->path, "/");
    }

    qtail = qhead;

    while (!in->stop && qtail) {
	dir = opendir (qtail->path);

	if (dir) {
	    while (!in->stop && (de = readdir (dir))) {
		switch (de->d_type) {
		case DT_DIR:
		    break;
		    if (!strcmp (".", de->d_name) ||
			!strcmp ("..", de->d_name)) {
			continue;
		    }

		    l = malloc (sizeof (struct bfs) +
				strlen (qtail->path) +
				1 +
				strlen (de->d_name) +
				1);
		    l->next = NULL;

		    strcpy (l->path, qtail->path);
		    strcat (l->path, de->d_name);
		    strcat (l->path, "/");

		    qhead->next = l;
		    qhead = l;

		    in->stop = in->func (l->path, in->usrdata);
		    break;

		case DT_BLK:
		case DT_CHR:
		case DT_FIFO:
		case DT_REG:
		case DT_SOCK:
		    strcpy (fpath, qtail->path);
		    strcat (fpath, de->d_name);

		    in->stop = in->func (fpath, in->usrdata);
		    break;

		case DT_LNK:
		    break;

		case DT_UNKNOWN:
		    break;

		default:
		    fprintf (stderr, "bad d_type\n");
		    exit (EXIT_FAILURE);
		}
	    }

	    closedir (dir);
	    dir = NULL;
	} else {
	    fprintf (stderr, "cannot open dir: %s\n", qtail->path);
	}

	l = qtail;
	qtail = l->next;

	free (l);
    }

    if (dir) {
	closedir (dir);
    }

    while (qtail) {
	l = qtail;
	qtail = l->next;

	free (l);
    }

    return NULL;
}

struct input *
input_browse (const char *root,
	      int (*func) (const char *, void *),
	      void *usrdata)
{
    struct input *in;

    in = calloc (1, sizeof (struct input) + strlen (root) + 1);
    in->func = func;
    in->usrdata = usrdata;
    strcpy (in->extra, root);

    if (pthread_create (&in->thread, NULL, browse_thread, in)) {
	perror (__func__);
	exit (EXIT_FAILURE);
    }

    return in;
}

static void *
read_thread (void *inp)
{
    struct input *in;
    char *line, buf[64];
    int len, maxlen, i;
    ssize_t readno;
    FILE *file;

    in = inp;
    maxlen = 16;
    len = 0;
    file = *(FILE **) in->extra;
    line = malloc (maxlen + 1);

    while (!in->stop && (readno = fread (buf, 1, sizeof (buf), file)) > 0) {
	printf ("readno: %ld\n", readno);

	if (maxlen < len + readno) {
	    maxlen = len + readno;
	    line = realloc (line, maxlen + 1);
	}

	memcpy (line + len, buf, readno);

	i = len;
	len += readno;

	do {
	    printf ("loop\n");
	    while (i < len) {
		if (line[i] == '\n') {
		    line[i] = 0;
		}

		if (line[i] == 0) {
		    in->stop = in->func (line, in->usrdata);
		    memmove (line, line + i, len - i);
		    len -= i;
		    i = 0;
		    continue;
		}

		i++;
	    }
	} while (!in->stop && i < len);
    }

    if (len > 0 && !in->stop) {
	line[len] = 0;
	printf ("the rest of line: %s\n", line);
	in->func (line, in->usrdata);
    }

    if (readno < 0) {
	perror (__func__);
	exit (EXIT_FAILURE);
    }

    free (line);

    return NULL;
}

struct input *
input_read (FILE *file,
	    int (*func) (const char *, void *),
	    void *usrdata)
{
    struct input *in;

    in = calloc (1, sizeof (struct input) + sizeof (FILE *));
    in->func = func;
    in->usrdata = usrdata;
    memcpy (in->extra, &file, sizeof (FILE *));

    if (pthread_create (&in->thread, NULL, read_thread, in)) {
	perror (__func__);
	exit (EXIT_FAILURE);
    }

    return in;
}

void
input_wait (struct input *in)
{
    pthread_join (in->thread, NULL);
    free (in);
}

void
input_close (struct input *in)
{
    in->stop = 1;

    input_wait (in);
}
