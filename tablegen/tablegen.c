#include "tablegen.h"
#include "graph.h"
#include "tree.h"
#if TABLE_TEST
#include "table.h"
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>
#include <wchar.h>
#include <wctype.h>
#include <string.h>
#include <locale.h>
#include <limits.h>
#include <assert.h>

enum
{
    CHR_BITS = 16,
    INS_BITS = 8,
    MUL_BITS = 14,
    SHI_BITS = 4,
    MOD_BITS = 4,
    OFF_BITS = 18,
};

_Static_assert (CHR_BITS + INS_BITS + MUL_BITS +
		SHI_BITS + MOD_BITS + OFF_BITS == 64);

static void
link_graph (struct graph *g, wchar_t a, wchar_t b, int c)
{
    int w;

    w = graph_get (g, a, b);

    if (c < w) {
	graph_set (g, a, b, c);
	//printf ("link %lc %lc %d : %d\n", a, b, c, w);
    }
}

static void
load_relations (struct graph *g, const char *line, bool anycase)
{
    const char *src;
    wchar_t *wr, *sp;
    size_t wl;
    long c;
    int k, i;
    
    src = line;
    wl = mbsrtowcs (NULL, &src, 0, NULL);
    assert (wl != (size_t) -1);
    wr = malloc (sizeof (wchar_t) * (wl + 1));
    wl = mbsrtowcs (wr, &src, wl + 1, NULL);
    assert (wl != (size_t) -1);

    sp = wcschr (wr, L' ');

    if (!sp) {
	fprintf (stderr, "bad format at line: %s\n", line);
	exit (EXIT_FAILURE);
    }

    c = wcstol (sp + 1, NULL, 10);
    assert (c);

    //printf ("%ls %lu %ld\n", wr, wl, c);

    for (k = 1; k < (sp - wr); k++) {
	for (i = 0; i < k; i++) {
	    link_graph (g, wr[k], wr[i], c);
	}
    }

    free (wr);
}

struct props
{
    int case_cost;
    int default_cost;
};

static void
load_caserange (struct graph *g, struct props *p, const char *line)
{
    const char *src;
    size_t wl;
    wchar_t wr[3], v;

    src = line;
    wl = mbsrtowcs (NULL, &src, 0, NULL);

    if (wl < 2) {
	fprintf (stderr, "[case-range] line must contain "
		 "two wide characters: %s\n", line);
	exit (EXIT_FAILURE);
    }

    wl = mbsrtowcs (wr, &src, 3, NULL);
    assert (wl >= 2);

    for (v = wr[0]; v <= wr[1]; v++) {
	if (v != towupper (v)) {
	    link_graph (g, v, towupper (v), p->case_cost);
	}
    }
}

static void
load_props (struct props *p, const char *line)
{
    if (!strncmp (line, "case=", strlen ("case="))) {
	p->case_cost = strtol (line + strlen ("case="), NULL, 10);
    } else if (!strncmp (line, "default=", strlen ("default="))) {
	p->default_cost = strtol (line + strlen ("default="), NULL, 10);
    } else {
	fprintf (stderr, "bad line: %s\n", line);
	exit (EXIT_FAILURE);
    }
}

struct node
{
    wchar_t v;
    int c;
    struct tree *l;
};

struct link
{
    wchar_t v;
    int c;
};

static int
compare_node (void *n0p, void *n1p)
{
    struct node *n0 = n0p, *n1 = n1p;

    return (int) n0->v - (int) n1->v;
}

static void
destroy_node (void *np)
{
    struct node *n = np;

    tree_destroy (n->l);
}

static void
print_node (void *np)
{
    struct node *n = np;

    printf ("%lc\n", n->v);

    tree_print (n->l, 0);
}

static int
compare_link (void *l0p, void *l1p)
{
    struct link *l0 = l0p, *l1 = l1p;

    return (int) l0->v - (int) l1->v;
}

static void
print_link (void *lp)
{
    struct link *l = lp;

    printf ("\t%lc %d\n", l->v, l->c);
}

static struct treenode *
create_link (void *vp)
{
    struct link l;

    l.v = *(wchar_t *) vp;
    l.c = INT_MAX;

    return treenode_create (&l, sizeof (l));
}

static struct treenode *
create_node (void *vp)
{
    struct node n;

    n.v = *(wchar_t *) vp;
    n.c = INT_MAX;
    n.l = tree_create (compare_link, NULL, print_link);

    return treenode_create (&n, sizeof (n));
}

static void
load_default (struct tree *t, struct props *p, const char *line)
{
    struct node *n;
    const char *src;
    wchar_t *wline, *split;
    size_t wlen, i;
    long cost;

    src = line;
    wlen = mbsrtowcs (NULL, &src, 0, NULL);
    assert (wlen != (size_t) -1);
    wline = malloc (sizeof (wchar_t) * (wlen + 1));
    wlen = mbsrtowcs (wline, &src, wlen + 1, NULL);
    assert (wlen != (size_t) -1);

    split = wcschr (wline, L' ');
    assert (split);

    cost = wcstol (split + 1, NULL, 10);
    assert (cost);

    for (i = 0; i < split - wline; i++) {
	n = tree_lazy (t, &wline[i], create_node, &wline[i]);
	n->c = cost;
    }

    free (wline);
}

static void
update_tree_one (struct tree *t, wchar_t v, wchar_t w, int c)
{
    struct node *n;
    struct link *l;

    if (v == w) {
	return;
    }

    n = tree_lazy (t, &v, create_node, &v);
    l = tree_lazy (n->l, &w, create_link, &w);

    assert (l->c == INT_MAX || l->c == c);

    if (c < l->c) {
	l->c = c;
    }
}

static void
update_tree (struct tree *t, wchar_t v, wchar_t w, int c)
{
    update_tree_one (t, v, w, c);
    update_tree_one (t, w, v, c);
}

static struct tree *
gentree (FILE *file, struct props *props)
{
    enum state
    {
	unknown,
	any_case,
	match_case,
	case_range,
	state_props,
	state_default,
    };

    struct graph *g;
    struct tree *t;
    int i, c;
    wchar_t v, e;
    char *line;
    size_t linesize;
    ssize_t ret;
    enum state state;

    state = unknown;
    line = NULL;
    linesize = 0;
    g = graph_create ();
    t = tree_create (compare_node, destroy_node, print_node);

    while ((ret = getline (&line, &linesize, file)) > 0) {
	if (!line || !strncmp (line, "//", strlen ("//"))) {
	    continue;
	} else if (!strncmp (line, "[props]", strlen ("[const]"))) {
	    state = state_props;
	} else if (!strncmp (line, "[default]", strlen ("[const]"))) {
	    state = state_default;
	} else if (!strncmp (line, "[any-case]", strlen ("[any-case]"))) {
	    state = any_case;
	} else if (!strncmp (line, "[match-case]", strlen ("[match-case]"))) {
	    state = match_case;
	} else if (!strncmp (line, "[case-range]", strlen ("[case-range]"))) {
	    state = case_range;
	} else {
	    switch (state) {
	    case any_case:
		load_relations (g, line, true);
		break;

	    case match_case:
		load_relations (g, line, false);
		break;

	    case case_range:
		load_caserange (g, props, line);
		break;

	    case state_props:
		load_props (props, line);
		break;

	    case state_default:
		load_default (t, props, line);
		break;

	    default:
		fprintf (stderr, "bad state %d\n", state);
		exit (EXIT_FAILURE);
	    }
	}
    }

    while (graph_size (g)) {
	v = graph_node (g, 0);

	graph_dijkstra (g, v);

	for (i = 0; i < graph_size (g); i++) {
	    e = graph_node (g, i);

	    if ((c = graph_dist (g, e)) < INT_MAX) {
		update_tree (t, v, e, c);
	    }
	}

	graph_remove (g, v);
    }

    graph_destroy (g);

    return t;
}

_Static_assert (sizeof (int) == sizeof (unsigned short) * 2);

static int
compare_entry (const void *e0p, const void *e1p)
{
    const struct entry *e0 = e0p, *e1 = e1p;

    return (int) e0->pos - (int) e1->pos;
}

static bool
etryresolve (struct entry *e)
{
    int i, p;
    bool *m;

    //printf ("etryresolve %u %u %u/%u\n", e->mul, e->shift, e->size, e->mod);

    m = alloca (sizeof (bool) * (1u << e->mod));
    memset (m, 0, sizeof (bool) * (1u << e->mod));

    for (i = 0; i < e->size; i++) {
	//printf ("check %lc %u\n", e->list[i].code, e->list[i].code);
	p = (e->list[i].code * e->mul >> e->shift) & ((1 << e->mod) - 1);

	if (m[p]) {
	    //printf ("next please\n");
	    return false;
	}

	m[p] = true;
    }

    for (i = 0; i < e->size; i++) {
	e->list[i].pos = (e->list[i].code * e->mul >> e->shift) &
	    ((1u << e->mod) - 1);
    }

    qsort (e->list, e->size, sizeof (struct entry), compare_entry);
    /*
    printf ("resolved %lc %2u %1u | %u/%u\n",
	    e->code, e->mul, e->shift, e->size, 1u << e->mod);

    for (i = 0; i < e->size; i++) {
	printf ("list %lc %2u | %u\n",
		e->list[i].code, e->list[i].cost, e->list[i].pos);
    }

    printf ("\n");
    */

    return true;
}

static void
eresolve (struct entry *e)
{
    e->mod = 0;

    while ((1u << e->mod) < e->size) {
	e->mod++;
    }

    while (true) {
	e->mul = 0;

	do {
	    for (e->shift = 0; e->shift < (1 << SHI_BITS); e->shift++) {
		if (etryresolve (e)) {
		    return;
		}
	    }
	} while (++e->mul < (1 << MUL_BITS));

	printf ("need to increase mod %u / %u (%u) -> %u (%u)\n",
		e->size, e->mod, 1u << e->mod,
		e->mod + 1, 1u << (e->mod + 1));
	e->mod++;

	if (e->mod >= (1u << MOD_BITS)) {
	    fprintf (stderr, "too big mod, "
		     "increase MOD_BITS\n");
	    exit (EXIT_FAILURE);
	}
    }
}

static struct entry *
flattentree (struct tree *t, struct props *p)
{
    struct node *n;
    struct link *l;
    struct entry *ev, *next, *nextend;
    int ni, li, ec;

    ec = 1 + tree_size (t);

    for (ni = 0; ni < tree_size (t); ni++) {
	n = tree_data (t, ni);
	assert (n);

	ec += tree_size (n->l);
    }

    ev = malloc (sizeof (struct entry) * ec);
    memset (ev, 0, sizeof (struct entry) * ec);

    next = ev + 1;
    nextend = ev + ec;

    ev->size = tree_size (t);
    ev->list = next;

    next += tree_size (t);

    for (ni = 0; ni < tree_size (t); ni++) {
	n = tree_data (t, ni);
	assert (n);

	ev->list[ni].code = n->v;
	ev->list[ni].cost = n->c < INT_MAX ?
	    n->c : p->default_cost;
	ev->list[ni].size = tree_size (n->l);
	ev->list[ni].list = next;

	next += tree_size (n->l);
	assert (next <= nextend);

	for (li = 0; li < tree_size (n->l); li++) {
	    l = tree_data (n->l, li);
	    assert (l);

	    ev->list[ni].list[li].code = l->v;
	    ev->list[ni].list[li].cost = l->c;
	}
    }

    assert (next == nextend);

    return ev;
}

struct entry *
gentable (FILE *file, int *dc)
{
    struct tree *t;
    struct entry *e;
    struct props p;
    int i;

    memset (&p, 0, sizeof (p));

    t = gentree (file, &p);

    if (!t) {
	return NULL;
    }

    e = flattentree (t, &p);

    eresolve (e);

    for (i = 0; i < e->size; i++) {
	eresolve (&e->list[i]);
    }

    if (dc) {
	*dc = p.default_cost;
    }

    return e;
}

void
gencode (struct entry *e, int dc, FILE *hfile, FILE *cfile)
{
    int i, pi, ei, ai;

    fprintf (hfile,
	     "extern const unsigned short codev[];\n");

    fprintf (cfile,
	     "const unsigned short codev[] = {\n");

    ei = 0;
    ai = 0;

    for (i = 0; i < e->size; i++) {
	for (pi = ei = 0; ei < e->list[i].size; ei++) {
	    while (pi < e->list[i].list[ei].pos) {
		fprintf (cfile,
			 "    0   , /* %lc */\n",
			 e->list[i].code);
		ai++;
		pi++;
	    }

	    fprintf (cfile,
		     "    %-4u, /* %lc -> %lc */\n",
		     e->list[i].list[ei].code,
		     e->list[i].code,
		     e->list[i].list[ei].code);

	    ai++;
	    pi++;
	}

	while (pi < (1u << e->list[i].mod)) {
	    fprintf (cfile,
		     "    0   , /* %lc */\n",
		     e->list[i].code);
	    ai++;
	    pi++;
	}
    }

    fprintf (cfile, "};\n");

    fprintf (hfile,
	     "extern const unsigned char costv[];\n");

    fprintf (cfile,
	     "const unsigned char costv[] = {\n");

    ei = 0;
    ai = 0;

    for (i = 0; i < e->size; i++) {
	for (pi = ei = 0; ei < e->list[i].size; ei++) {
	    while (pi < e->list[i].list[ei].pos) {
		fprintf (cfile,
			 "    0   , /* %lc */\n",
			 e->list[i].code);
		ai++;
		pi++;
	    }

	    fprintf (cfile,
		     "    %-4u, /* %lc -> %lc */\n",
		     e->list[i].list[ei].cost,
		     e->list[i].code,
		     e->list[i].list[ei].code);

	    ai++;
	    pi++;
	}

	while (pi < (1u << e->list[i].mod)) {
	    fprintf (cfile,
		     "    0   , /* %lc */\n",
		     e->list[i].code);
	    ai++;
	    pi++;
	}
    }

    fprintf (cfile, "};\n");

    fprintf (hfile,
	     "enum\n"
	     "{\n"
	     "    REDIRECT_MUL=%d,\n"
	     "    REDIRECT_SHI=%d,\n"
	     "    REDIRECT_MOD=%d,\n"
	     "    DEFAULT_COST=%d,\n"
	     "};\n"
	     "struct redirect\n"
	     "{\n"
	     "    unsigned code:%u;\n"
	     "    unsigned ins:%u;\n"
	     "    unsigned mul:%u;\n"
	     "    unsigned shi:%u;\n"
	     "    unsigned mod:%u;\n"
	     "    unsigned off:%u;\n"
	     "};\n"
	     "extern const struct redirect redirectv[];\n",
	     e->mul, e->shift, e->mod, dc,
	     CHR_BITS, INS_BITS,
	     MUL_BITS, SHI_BITS,
	     MOD_BITS, OFF_BITS);

    fprintf (cfile,
	     "struct redirect\n"
	     "{\n"
	     "    unsigned code:%u;\n"
	     "    unsigned ins:%u;\n"
	     "    unsigned mul:%u;\n"
	     "    unsigned shi:%u;\n"
	     "    unsigned mod:%u;\n"
	     "    unsigned off:%u;\n"
	     "};\n"
	     "const struct redirect redirectv[] = {\n",
	     CHR_BITS, INS_BITS,
	     MUL_BITS, SHI_BITS,
	     MOD_BITS, OFF_BITS);

    ei = 0;

    for (pi = i = 0; i < e->size; i++) {
	while (pi < e->list[i].pos) {
	    fprintf (cfile,
		     "   {    0,   0,   0,  0, 0,     0 }, /* %lc */\n",
		     e->list[i].code);
	    pi++;
	}

	assert (pi == e->list[i].pos);

	fprintf (cfile,
		 "   { %4u, %3u, %3u, %2u, %1u, %5u }, /* %lc */\n",
		 e->list[i].code,
		 e->list[i].cost,
		 e->list[i].mul,
		 e->list[i].shift,
		 e->list[i].mod,
		 ei,
		 e->list[i].code);
	pi++;
	ei += 1u << e->list[i].mod;
    }

    /* there may be need to fill the end of array with 0s */
    assert (pi == (1u << e->mod));

    fprintf (cfile, "};\n");
}
