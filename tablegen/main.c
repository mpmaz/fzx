#include "tablegen.h"

#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <errno.h>
#include <locale.h>

int
main (int argc, char **argv)
{
    struct entry *e;
    char *ipath, *hpath, *cpath;
    FILE *ifile, *hfile, *cfile;
    int c, ret, dc;

    if (!setlocale (LC_ALL, "C.UTF8")) {
	perror ("setlocale");
	return EXIT_FAILURE;
    }

    e = NULL;
    ret = EXIT_SUCCESS;
    ipath = NULL;
    hpath = NULL;
    cpath = NULL;
    ifile = NULL;
    hfile = NULL;
    cfile = NULL;

    while ((c = getopt (argc, argv, "i:h:c:")) != -1) {
	switch (c) {
	case 'i':
	    ipath = strdup (optarg);
	    break;

	case 'h':
	    hpath = strdup (optarg);
	    break;

	case 'c':
	    cpath = strdup (optarg);
	    break;

	default:
	    fprintf (stderr, "unknown option: %c\n", c);
	}
    }

    if (!ipath) {
	fprintf (stderr, "missing -i option\n");
	ret = EXIT_FAILURE;
	goto cleanup;
    }

    ifile = fopen (ipath, "r");

    if (!ifile) {
	fprintf (stderr, "cannot open %s file for reading: %s\n",
		 ipath, strerror (errno));
	ret = EXIT_FAILURE;
	goto cleanup;
    }

    if (hpath) {
	hfile = fopen (hpath, "w");

	if (!hfile) {
	    fprintf (stderr, "cannot open %s file for reading: %s\n",
		     hpath, strerror (errno));
	    goto cleanup;
	}
    }

    if (cpath) {
	cfile = fopen (cpath, "w");

	if (!cfile) {
	    fprintf (stderr, "cannot open %s file for reading: %s\n",
		     cpath, strerror (errno));
	    goto cleanup;
	}
    }

    e = gentable (ifile, &dc);

    if (hfile || cfile) {
	gencode (e, dc, hfile, cfile);
    }

cleanup:
    if (e) {
	free (e);
    }

    if (ifile && ipath) {
	fclose (ifile);
    }

    if (hfile && hpath) {
	fclose (hfile);
    }

    if (cfile && cpath) {
	fclose (cfile);
    }

    if (ipath) {
	free (ipath);
    }

    if (hpath) {
	free (hpath);
    }

    if (cpath) {
	free (cpath);
    }

    return ret;
}
