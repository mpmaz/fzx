#pragma once

#include <stdio.h>
#include <wchar.h>

struct entry
{
    wchar_t code;
    unsigned cost, size, mod, mul, shift, pos;
    struct entry *list;
};

struct entry *gentable (FILE *file, int *dc);
void gencode (struct entry *e, int dc,
	      FILE *hfile, FILE *cfile);
