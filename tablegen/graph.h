#pragma once

struct graph;

struct graph *
graph_create ();

void
graph_destroy (struct graph *g);

void
graph_print (struct graph *g);

void
graph_set (struct graph *g, int a, int b, int w);

int
graph_get (struct graph *g, int a, int b);

int
graph_dist (struct graph *g, int v);

void
graph_remove (struct graph *g, int v);

int
graph_size (struct graph *g);

int
graph_node (struct graph *g, int i);

void
graph_dijkstra (struct graph *g, int v);
