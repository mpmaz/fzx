#include "graph.h"
#include "tree.h"

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <assert.h>

struct graph
{
    struct tree *t;
};

struct node
{
    int v, d, h;
    struct tree *l;
};

struct link
{
    struct node *n;
    int w;
};

static void
destroy_node (void *np)
{
}

static void
print_node (void *np)
{
    struct node *n = np;

    printf ("%lc:\n", (wchar_t) n->v);

    tree_print (n->l, 0);
}

static int
compare_node (void *n0p, void *n1p)
{
    struct node *n0 = n0p, *n1 = n1p;

    return n0->v - n1->v;
}

struct graph *
graph_create ()
{
    struct graph *g;

    g = malloc (sizeof (struct graph));
    g->t = tree_create (compare_node, destroy_node, print_node);

    return g;
}

void
graph_destroy (struct graph *g)
{
    tree_destroy (g->t);
    free (g);
}

void
graph_print (struct graph *g)
{
    tree_print (g->t, 0);
}

static int
compare_link (void *l0p, void *l1p)
{
    struct link *l0 = l0p, *l1 = l1p;

    return l0->n->v - l1->n->v;
}

static void
destroy_link (void *lp)
{
}

static void
print_link (void *lp)
{
    struct link *l = lp;

    printf ("  -> %lc : %d\n", (wchar_t) l->n->v, l->w);
}

static struct treenode *
make_node (void *d)
{
    struct node n;

    n.v = *(int *) d;
    n.d = 0,
    n.l = tree_create (compare_link, destroy_link, print_link);

    return treenode_create (&n, sizeof (n));
}

static struct treenode *
make_link (void *n)
{
    struct link l;

    l.n = n;
    l.w = 0;

    return treenode_create (&l, sizeof (l));
}

static void
set (struct graph *g, int a, int b, int w)
{
    struct node *n, *m;
    struct link *l;

    n = tree_lazy (g->t, &a, make_node, &a);
    assert (n);
    assert (n->v == a);

    m = tree_lazy (g->t, &b, make_node, &b);
    assert (m);
    assert (m->v == b);

    l = tree_lazy (n->l, &m, make_link, m);
    assert (l);
    assert (l->n == m);

    l->w = w;
}

void
graph_set (struct graph *g, int a, int b, int w)
{
    set (g, a, b, w);
    set (g, b, a, w);
}

int
graph_get (struct graph *g, int a, int b)
{
    struct node *n, *m;
    struct link *l;

    if (!(n = tree_find (g->t, &a, NULL))) {
	return INT_MAX;
    }

    assert (n->v == a);

    if (!(m = tree_find (g->t, &b, NULL))) {
	return INT_MAX;
    }

    assert (m->v == b);

    if (!(l = tree_find (n->l, &m, NULL))) {
	return INT_MAX;
    }

    assert (l->n == m);

    return l->w;
}

int
graph_dist (struct graph *g, int v)
{
    struct node *n;

    if (!(n = tree_find (g->t, &v, NULL))) {
	return INT_MAX;
    }

    assert (n->v == v);

    return n->d;
}

static void
remove_link (void *lp, void *np)
{
    struct link *l = lp;
    struct node *n = np;
    size_t i;

    if (!tree_find (l->n->l, &n, &i)) {
	printf ("not found :/\n");
	exit (EXIT_FAILURE);
    }

    tree_delete (l->n->l, i);
}

void
graph_remove (struct graph *g, int v)
{
    struct node *n;
    struct link *l;
    size_t i, k;

    if (!(n = tree_find (g->t, &v, &i))) {
	return;
    }

    tree_foreach (n->l, remove_link, n);
    tree_delete (g->t, i);

    for (i = 0; i < tree_size (g->t); i++) {
	n = tree_data (g->t, i);
	assert (n->v != v);

	for (k = 0; k < tree_size (n->l); k++) {
	    l = tree_data (n->l, k);
	    assert (l->n->v != v);
	}
    }
}

int
graph_size (struct graph *g)
{
    return tree_size (g->t);
}

int
graph_node (struct graph *g, int i)
{
    struct node *n;

    if ((n = tree_data (g->t, i))) {
	return n->v;
    }

    return -1;
}

struct order
{
    struct node *n;
    int d;
};

struct heap
{
    struct node **v;
    int c;
};

static void
build_heap (void *np, void *ctxp)
{
    void **ctx;
    struct heap *h;
    struct node *n;
    int b, i, k;

    ctx = ctxp;
    n = np;
    h = ctx[0];
    b = *(int *) ctx[1];

    n->h = h->c;
    n->d = n->v == b ? 0 : INT_MAX;
    h->v[i = h->c++] = n;

    while (i && h->v[i]->d < h->v[k = (i - 1) / 2]->d) {
	n = h->v[i];

	h->v[i] = h->v[k];
	assert (h->v[i]->h == k);
	h->v[i]->h = i;

	h->v[k] = n;
	assert (h->v[k]->h == i);
	n->h = i = k;
    }
}

static void
assert_heap (struct heap *h, int i)
{
    if (i >= h->c) {
	return;
    }

    assert (h->v[i]->h == i);

    if (i && h->v[(i - 1) / 2]->d > h->v[i]->d) {
	fprintf (stderr, "invalid heap %d (%d) > %d (%d)\n",
		 (i - 1) / 2, h->v[(i - 1) / 2]->d,
		 i, h->v[i]->d);
	abort ();
    }

    assert_heap (h, i * 2 + 1);
    assert_heap (h, i * 2 + 2);
}

static void
heap_up (struct heap *h, int i)
{
    struct node *n;
    int k;

    assert (i >= 0);
    assert (i < h->c);

    while ((k = i * 2 + 1) < h->c) {
	assert (h->v[i]->h == i);
	assert (h->v[k]->h == k);

	if (k + 1 < h->c) {
	    if (h->v[i]->d <= h->v[k]->d && h->v[i]->d <= h->v[k + 1]->d) {
		break;
	    }

	    if (h->v[k + 1]->d < h->v[k]->d && h->v[k + 1]->d < h->v[i]->d) {
		k++;
	    }
	} else {
	    if (h->v[i]->d <= h->v[k]->d) {
		break;
	    }
	}

	assert (h->v[k]->d < h->v[i]->d);

	n = h->v[k];
	h->v[k] = h->v[i];
	h->v[i] = n;

	h->v[k]->h = k;
	h->v[i]->h = i;

	i = k;
    }

    assert_heap (h, 0);
}

static void
heap_down (struct heap *h, int i)
{
    struct node *n;
    int k;

    while (i) {
	k = (i - 1) / 2;

	if (h->v[k]->d <= h->v[i]->d) {
	    break;
	}

	n = h->v[k];
	h->v[k] = h->v[i];
	h->v[i] = n;

	h->v[k]->h = k;
	h->v[i]->h = i;

	i = k;
    }

    assert_heap (h, 0);
}

static void
reduce_heap (struct heap *h)
{
    assert (h->c);
    assert (h->v[0]->h == 0);
    assert (h->v[h->c - 1]->h == h->c - 1);
    assert_heap (h, 0);

    h->v[0]->h = -1;
    h->v[0] = h->v[--h->c];
    h->v[0]->h = 0;

    if (h->c) {
	heap_up (h, 0);
    }
}

static void
update_dist (void *lp, void *ctxp)
{
    void **ctx;
    struct link *l;
    struct node *n;
    struct heap *h;

    ctx = ctxp;
    l = lp;
    n = ctx[0];
    h = ctx[1];

    if (n->d + l->w < l->n->d) {
	assert (l->n->h != -1);
	l->n->d = n->d + l->w;
	heap_down (h, l->n->h);
    }
}

void
graph_dijkstra (struct graph *g, int v)
{
    struct heap h;
    struct node *n;
    void *ctx[2];

    h.v = malloc (sizeof (struct node *) * tree_size (g->t));
    h.c = 0;

    //tree_print (g->t, 0);

    memset (h.v, 0, sizeof (struct node *) * tree_size (g->t));

    ctx[0] = &h;
    ctx[1] = &v;

    tree_foreach (g->t, build_heap, ctx);

    while (h.c) {
	n = h.v[0];
	assert (n->h == 0);
	reduce_heap (&h);

	if (n->d < INT_MAX) {
	    ctx[0] = n;
	    ctx[1] = &h;

	    tree_foreach (n->l, update_dist, ctx);
	}
    }

    free (h.v);
}
