#include "tty.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <assert.h>
#include <sys/ioctl.h>
#include <sys/select.h>

#define BUFFER_SIZE 16

struct tty
{
    struct termios term;
    int fd;

    volatile int run;

    void (*feed) (int, void *);
    void *data;
    char esc[BUFFER_SIZE + 1];

    pthread_t thread;
    pthread_mutex_t lock;
    pthread_cond_t cond;
};

static int
readbyte (struct tty *t, char *cp)
{
    struct timeval tv;
    fd_set fds;
    int ret;
    char c;

    tv.tv_sec = 0;
    tv.tv_usec = 5000 * 1000;

    FD_ZERO (&fds);
    FD_SET (t->fd, &fds);

    ret = select (t->fd + 1, &fds, NULL, NULL, &tv);

    if (!ret) {
	printf ("select failed\n");
	return 0;
    }

    ret = read (t->fd, &c, 1);

    if (ret < 0 && ret != EAGAIN && t->run) {
	perror ("tty/read");
	exit (EXIT_FAILURE);
    }

    *cp = c;

    return 1;
}

static int
escape (struct tty *t, char *in, int *len)
{
    int i;

    assert (in[0] == 0x1b);

    for (i = 1; i < BUFFER_SIZE; i++) {
	if (i >= *len && !readbyte (t, &in[(*len)++])) {
	    return 0;
	}

	if (in[i] >= 'A' && in[i] <= 'Z') {
	    i++;
	    break;
	}
    }

    if (i == BUFFER_SIZE) {
	return 0;
    }

    memcpy (t->esc, in, i);
    t->esc[i + 1] = 0;
    pthread_cond_signal (&t->cond);

    return i;
}

static void *
thread (void *tp)
{
    struct tty *t;
    int ret, esc, i;
    char buf[BUFFER_SIZE];
    fd_set fds;

    t = tp;

    FD_ZERO (&fds);
    FD_SET (t->fd, &fds);

    while (t->run) {
	ret = select (t->fd + 1, &fds, NULL, NULL, NULL);

	if (ret < 0) {
	    perror ("select");
	}

	ret = read (t->fd, buf, sizeof (buf));

	if (ret < 0 && ret != EAGAIN && t->run) {
	    perror ("tty/read");
	    exit (EXIT_FAILURE);
	}

	for (i = 0; i < ret; i++) {
	    if (buf[i] == 0x1b) {
		memmove (buf, buf + i, ret - i);
		ret -= i;
		i = 0;

		if ((esc = escape (t, buf, &ret))) {
		    i += esc;
		    continue;
		}
	    }

	    t->feed (buf[i], t->data);
	}
    }

    return NULL;
}

struct tty *
tty_open (const char *dev,
	  void (*feed) (int, void *),
	  void *data)
{
    struct tty *t;
    struct termios term;

    t = malloc (sizeof (struct tty));
    t->fd = open (dev, O_RDWR | O_NDELAY);
    t->feed = feed;
    t->data = data;

    if (t->fd < 0) {
	fprintf (stderr, "can't open %s\n", dev);
	exit (EXIT_FAILURE);
    }

    if (tcgetattr (t->fd, &t->term)) {
	perror ("tcgetattr");
	exit (EXIT_FAILURE);
    }

    term = t->term;
    term.c_lflag &= ~(ICANON | ECHO | ISIG);

    if (tcsetattr (t->fd, TCSANOW, &term)) {
	perror ("tcsetattr");
	exit (EXIT_FAILURE);
    }

    t->run = 1;

    if (pthread_create (&t->thread, NULL, thread, t)) {
	perror (__func__);
	exit (EXIT_FAILURE);
    }

    pthread_mutex_init (&t->lock, NULL);
    pthread_cond_init (&t->cond, NULL);

    return t;
}

void
tty_close (struct tty *t)
{
    t->run = 0;

    if (tcsetattr (t->fd, TCSANOW, &t->term)) {
	perror ("tcsetattr");
    }

    close (t->fd);

    pthread_mutex_destroy (&t->lock);
    pthread_cond_destroy (&t->cond);
}

void
tty_getsize (struct tty *t, int *w, int *h)
{
    struct winsize ws;

    if (ioctl (t->fd, TIOCGWINSZ, &ws) < 0) {
	*w = 80;
	*h = 25;
    } else {
	*w = ws.ws_col;
	*h = ws.ws_row;
    }
}

static void
writebytes (struct tty *t, const char *buf, size_t len)
{
    int ret;

    do {
	ret = write (t->fd, buf, len);
    } while (ret < 0 && errno == EAGAIN);

    if (ret != len && t->run) {
	perror (__func__);
	exit (EXIT_FAILURE);
    }
}

void
tty_getpos (struct tty *t, int *x, int *y)
{
    char *buf;

    *x = 0;
    *y = 0;

    pthread_mutex_lock (&t->lock);
    writebytes (t, "\033[6n", strlen ("\033[6n") + 1);
    pthread_cond_wait (&t->cond, &t->lock);

    buf = t->esc + 1;

    assert (buf[0] == '[');

    buf = buf + 1;
    *y = strtol (buf, &buf, 10) - 1;

    assert (*buf == ';');

    *x = strtol (buf + 1, &buf, 10) - 1;

    assert (*buf == 'R');

    pthread_mutex_unlock (&t->lock);
}

void
tty_printf (struct tty *t, const char *fmt, ...)
{
    va_list args;
    char *buf;
    int siz, ret;

    va_start (args, fmt);
    siz = vsnprintf (NULL, 0, fmt, args);
    va_end (args);

    buf = alloca (siz + 1);

    va_start (args, fmt);
    ret = vsnprintf (buf, siz + 1, fmt, args);
    va_end (args);

    assert (ret == siz);

    pthread_mutex_lock (&t->lock);
    writebytes (t, buf, siz + 1);
    pthread_mutex_unlock (&t->lock);
}

void
tty_puts (struct tty *t, const char *str)
{
    pthread_mutex_lock (&t->lock);
    writebytes (t, str, strlen (str) + 1);
    pthread_mutex_unlock (&t->lock);
}

void
tty_scroll (struct tty *t, int scroll)
{
    while (scroll-- > 0) {
	tty_puts (t, "\033[1S");
    }

    while (scroll++ < 0) {
	tty_puts (t, "\033[1T");
    }
}

void
tty_move (struct tty *t, int move)
{
    while (move-- > 0) {
	tty_puts (t, "\033[1A");
    }

    while (move++ < 0) {
	tty_puts (t, "\033[1B");
    }
}

void
tty_clearline (struct tty *t)
{
    tty_puts (t, "\033[K");
}

void
tty_column (struct tty *t, int col)
{
    tty_printf (t, "\033[%dG", col + 1);
}
